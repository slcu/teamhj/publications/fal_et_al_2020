# The only thing this file really needs to do is to define the variables:
#
# pathApplyTrsf - path to binary.
# pathBlockmatching - path to binary.
# ALTParameter - an integer, typically 3-5.
#    In our analyses, the ALTParameter was 3 for everything except the tracking
#    of SUN1_p18 from the second to the third timepoint, where it was 5.
# t0Path - path to first intensity image.
# t1Path - path to next intensity image.
# seg0Path - path to first intensity image.
# seg1Path - path to next intensity image.
# savePath - path folder where everything will be saved.
# matchingNamePostfix - a string used when saving files to signify parameters,
#     e.g. "t1_to_t2_ALT3"

# This file is not run on its own but is rather loaded when you run tracking.py.

import os

pathApplyTrsf = "/home/yassin/devCode/vt/build/bin/applyTrsf"
pathBlockmatching = "/home/yassin/devCode/vt/build/bin/blockmatching"

plant_name = "SUN1_p23"
ALTParameter = 3  # 3 - 5

### raw intensity images
root_path = "/home/Niklas/home3/projects/JoseNuclearShape/img_test/"
t0Path = root_path + plant_name + "/images/t2/C2-crop_300715_Plant23T1.tif"
t1Path = root_path + plant_name + "/images/t3/C2-crop_300715_Plant23T2.tif"

seg0Path = root_path + plant_name + "/images/t2/C2-crop_300715_Plant23T1_hmin_2_asf_2.00_s_2.00.tif"
seg1Path = root_path + plant_name + "/images/t3/C2-crop_300715_Plant23T2_hmin_2_asf_2.00_s_2.00.tif"

### Direct where to save the output.
timePoints = [
    os.path.split(os.path.split(t0Path)[0])[1],
    os.path.split(os.path.split(t1Path)[0])[1]
]
matchingNamePostfix = "{}_to_{}_ALT{}".format(timePoints[0], timePoints[1], ALTParameter)
mainPath = os.path.join(root_path, plant_name, "tracking", "")
if not os.path.exists(mainPath + matchingNamePostfix):
    if not os.path.exists(mainPath):
        os.mkdir(mainPath)
    os.mkdir(os.path.join(mainPath, matchingNamePostfix))

savePath = os.path.join(mainPath, matchingNamePostfix, "")
