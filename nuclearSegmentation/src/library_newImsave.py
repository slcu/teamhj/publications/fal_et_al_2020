# from tifffile import imread, imsave
from openalea.image.all import imread, imsave
from openalea.image.spatial_image import SpatialImage
import numpy as np
import scipy.ndimage
from scipy import ndimage
import libtiff
from scipy.ndimage.morphology import binary_dilation
from libtiff import *
from openalea.image.algo.analysis import SpatialImageAnalysis
from openalea.image.algo.basic import logicalnot
from vplants.asclepios.vt_exec.connexe import hysteresis, connected_components
from vplants.asclepios.vt_exec.morpho import dilation, erosion
from vplants.asclepios.vt_exec.recfilters import recfilters
from vplants.asclepios.vt_exec.regionalmax import regionalmax
from vplants.asclepios.vt_exec.watershed import watershed
from vplants.mars_alt.mars import segmentation
from vplants.mars_alt.mars.all import automatic_linear_parameters, \
    automatic_non_linear_parameters, fuse_reconstruction, reconstruct, \
    reconstruction_task, surface_landmark_matching_parameters
from vplants.mars_alt.mars.reconstruction import im2surface, surface2im
from tissuelab.tltiff import tiffread, tiffsave, getTiffFileResolution
from tissuelab.image import makeImageLabelsConsecutive
import os
import sys
from time import time
import scipy.ndimage as nd
from scipy.ndimage.filters import gaussian_filter
# from removeCells import removeTopCells
import matplotlib.pyplot as plt
import cPickle
from tissuelab.tltiff import tiffread, tiffsave

pathApplyTrsf = "~/devCode/vt/build/bin/applyTrsf"

# tiffsave(makeImageLabelsConsecutive(seg)[0], segImageFName, **tags)
# image, tags = tiffread(imageFName)


def calculateWalls(fileName, wallWidth=2):
    imr, tags = tiffread(fileName)
    tissueImageData = np.asfortranarray(imr)
    imageLaplace = scipy.ndimage.laplace(tissueImageData)
    walls = np.empty(tissueImageData.shape, dtype=bool)
    walls.fill(True)
    walls[imageLaplace == 0] = False
    if wallWidth > 2:
        walls = scipy.ndimage.morphology.binary_dilation(walls, iterations=wallWidth / 2 - 2).astype(walls.dtype)
    np.save(fileName[:-4] + "_walls", walls)


def rotateImage(fName):
    image, tags = tiffread(fName)
    image = ndimage.rotate(image, -90)
    image[image == 0] = 1
    tiffsave(image, fName[:-4] + "_rot.tif", **tags)


def fillHoles(mainImageFName, candidateImageFName, holeCenters):
    main, mtags = tiffread(mainImageFName)
    can, ctags = tiffread(candidateImageFName)
#     main = imread(mainImageFName)
#     can = imread(candidateImageFName)
    unfilled = []
    counter = 0
    for hCenter in holeCenters:
        holeCenter = [hCenter[2], hCenter[1], hCenter[0]]
        print holeCenter

        print main[holeCenter[0], holeCenter[1], holeCenter[2]], can[holeCenter[0], holeCenter[1], holeCenter[2]]
        print float(counter) / len(holeCenters)
        counter += 1
        if main[holeCenter[0], holeCenter[1], holeCenter[2]] == 1 and can[holeCenter[0], holeCenter[1], holeCenter[2]] != 1:
            w = np.where(can == can[holeCenter[0], holeCenter[1], holeCenter[2]])
            mainImageMaxLabel = np.max(main)
            mainImageMaxLabel += 1

            for i in xrange(len(w[0])):
                if main[w[0][i], w[1][i], w[2][i]] == 1:
                    main[w[0][i], w[1][i], w[2][i]] = mainImageMaxLabel
        elif main[holeCenter[0], holeCenter[1], holeCenter[2]] == 1 and can[holeCenter[0], holeCenter[1], holeCenter[2]] == 1:
            unfilled.append(holeCenter)
#     imsave(mainImageFName[:-4] + "_hf.tif", main)
    tiffsave(main, mainImageFName[:-4] + "_hf.tif", **mtags)
    print "unfilled = ", unfilled
    return unfilled


def removeL1Save(fName, backgroundLabel, checkCellsOnZLast=True):
    image, tags = tiffread(fName)
    maxLabel = np.max(image)
    backgroundLabelVoxels = (image == backgroundLabel)
    backgroundLabelVoxelsDil = binary_dilation(backgroundLabelVoxels, iterations=1)
    externalVoxels = image[backgroundLabelVoxelsDil - backgroundLabelVoxels]
    L1LabelsSet = set(np.unique(externalVoxels))
    if checkCellsOnZLast:
        L1LabelsSet.update(set(np.unique(image[..., -1])))
    if backgroundLabel in L1LabelsSet:
        L1LabelsSet.remove(backgroundLabel)
    L1Labels = list(L1LabelsSet)
    L1Characteristic = np.empty((maxLabel + 1, ), bool)
    L1Characteristic.fill(False)
    L1Characteristic[L1Labels] = True
    L1CellsVoxels = L1Characteristic[image]
    image[L1CellsVoxels] = backgroundLabel
    tiffsave(image, fName[:-4] + "_L1Removed.tif", **tags)


def segment(imageFName, hmin, asf, sigma, resolution=(1, 1, 1), checkBackground=True, background=1):
    image, tags = tiffread(imageFName)
    print tags
    im_filtered = gaussian_filter(image, sigma)
    im_filtered = segmentation.filtering(im_filtered, filter_type="asf", filter_value=asf)
    im_tmp = logicalnot(im_filtered)
    im_tmp = regionalmax(im_tmp, hmin)
    im_tmp = hysteresis(im_tmp, 1, hmin, connectivity=6)
    seeds = connected_components(im_tmp, 1)
    seg = watershed(seeds, im_filtered)
    seg.resolution = resolution
    if checkBackground:
        maxLabel = np.max(seg)
        print "Checking the background label ..."
        largestCellLabel = np.bincount(seg.flatten()).argmax()
        if largestCellLabel != background:
            np.putmask(seg, seg == background, maxLabel + 1)
            maxLabel += 1
            np.putmask(seg, seg == largestCellLabel, background)
    segImageFName = imageFName[:-4] + "_hmin_%d_asf_%1.2f_s_%1.2f.tif" % (hmin, asf, sigma)
    tiffsave(makeImageLabelsConsecutive(seg)[0], segImageFName, **tags)
    return segImageFName


def clean(segImageName, radius):
    radius = str(radius)
    pathfilter = "/home/yassin/devCode/vt_clean/vt_copie/libvp/bin/linux/cellfilter "
    pathskiz = "/home/yassin/devCode/vt_clean/vt_copie/vt-exec/bin/linux/skiz "
    # workspacePath = "/home/jose/workspace"
    workspacePath = "/tmp/"
    cleanImageName = segImageName[:-4] + "_clean_%s.tif" % (radius)
    im, tags = tiffread(segImageName)
    imsave("%s/segmented_%s.inr.gz" % (workspacePath, radius), SpatialImage(im))
    path_input = "%s/segmented_%s.inr.gz" % (workspacePath, radius)
    path_output = "%s/segmented_corrected_%s.inr.gz" % (workspacePath, radius)
    os.system(pathfilter + " " + path_input + " %s/cellfilter_%s.inr.gz -ouv -radius " % (workspacePath, radius) + radius + " -chamfer")
    os.system(pathskiz + " %s/cellfilter_%s.inr.gz " % (workspacePath, radius) + path_output)
    cleanImage = imread(path_output)
    tiffsave(makeImageLabelsConsecutive(cleanImage)[0], cleanImageName, **tags)


def generateNextColormap(currentColormapFName, matchingScoresFName, tFirstToRemove=[], background=1):
    fobj = file(matchingScoresFName)
    (matching, scoresList) = cPickle.load(fobj)
    fobj.close()
    fobj = file(currentColormapFName)
    cmap = cPickle.load(fobj)
    fobj.close()
    divisions = dict()
    for (d, m) in matching:
        divisions.setdefault(m, []).append(d)
    counter = 0

#     print divisions[1]
#     exit()
    for m, dList in divisions.iteritems():
        if len(dList) > 1:
            counter += 1
    newColormap = np.array(cmap)
    for m, dList in divisions.iteritems():
        if m in tFirstToRemove:
            for d in dList:
                newColormap[d] = background
        else:
            for d in dList:
                newColormap[d] = cmap[m]
    return newColormap


def detectIntensityChange(fName, epsilon=1.2):
    im, tags = tiffread(fName)
    intensityChangingSlices = []
    for z in xrange(im.shape[2] - 1):
        v1 = np.sum(im[..., z + 1])
        if np.sum(im[..., z + 1]) != 0:
            val = np.sum(im[..., z]) / float(v1)
        if val > epsilon or val < (1 / epsilon):
            intensityChangingSlices.append([z, float(val)])
    return intensityChangingSlices


def mergeCells(fname, cellLabelToDisappearTargetCellLabelList):
    im, tags = tiffread(fname)
    for [cellLabelToDisappear, targetCellLabel]in cellLabelToDisappearTargetCellLabelList:
        print [cellLabelToDisappear, targetCellLabel]
        np.putmask(im, im == cellLabelToDisappear, targetCellLabel)
    tiffsave(im, fname[:-4] + "_cm.tif", **tags)


def checkBG(fName, background=1):
    im, tags = tiffread(fName)
    maxLabel = np.max(im)
    print "Checking the background label ..."
    largestCellLabel = np.bincount(im.flatten()).argmax()
    if largestCellLabel != background:
        np.putmask(im, im == background, maxLabel + 1)
        maxLabel += 1
        np.putmask(im, im == largestCellLabel, background)
    tiffsave(im, fName, **tags)


def applySuccessiveTrsf(floatingImageName, trsfList, savePath, type="intensity"):
    """
    type is either intensity or segmented.
    """
    interpolation = "linear" if type == "intensity" else "nearest"
    fImage = imread(floatingImageName)
    counter = 0
    imName = savePath + floatingImageName.split("/")[-1][:-4] + ".inr"
    imsave(imName, fImage)
    for trsf in trsfList:
        nextImName = savePath + floatingImageName.split("/")[-1][:-4] + "_on_t%d.inr" % (counter + 1)
        print nextImName
        os.system(pathApplyTrsf + " " + imName + " " + nextImName + " -trsf %s" % trsf + " -%s" % interpolation)
        imName = nextImName
        counter += 1


def calculateGradiantInXY(imageFName):
    image, tags = tiffread(imageFName)  # problem, in new tiffread the first dimension is z
    shape = image.shape
    yVals = []
    xVals = []
    for z in xrange(shape[2]):
        print z
        valsL = []
        for i in xrange(shape[1] - 1):
            val = np.sum(np.sum(np.abs(image[i, :, z] - image[i + 1, :, z]), axis=0), axis=0)
            print val
            valsL.append(val)
        yVals.append(max(valsL))

    return xVals, yVals

def removeLargeCells(fName, maxVoxelNb, background = 1):
    image, tags = tiffread(fName)
    binCount = np.bincount(image.flatten())
    print len(list(np.where(binCount > maxVoxelNb)[0])), list(np.where(binCount > maxVoxelNb)[0])
    for cid in np.where(binCount > maxVoxelNb)[0]:
        print cid
        if cid != background:
            image[image == cid] = background
    newName = fName[:-4] + "_bigCellsRemoved_{}.tif".format(maxVoxelNb)
    tiffsave(image, newName, **tags)
    return newName


def segmentTwoChannel(imageFName, secondImageFName, hmin, asf, sigma, hmin_2, asf_2, sigma_2, resolution=(1, 1, 1), checkBackground=True, background=1):
    image, mtags = tiffread(imageFName)
    secondImage, stags = tiffread(secondImageFName)
    im_filtered = gaussian_filter(image, sigma)
    im_filtered = segmentation.filtering(im_filtered, filter_type="asf", filter_value=asf)
    im_filtered2 = gaussian_filter(secondImage, sigma_2)
    im_filtered2 = segmentation.filtering(im_filtered2, filter_type="asf", filter_value=asf_2)
    im_tmp = logicalnot(im_filtered)
    im_tmp = regionalmax(im_tmp, hmin)
    im_tmp = hysteresis(im_tmp, 1, hmin, connectivity=6)
    print "here"
    seeds = connected_components(im_tmp, 1)
    seg = watershed(seeds, im_filtered2)
#     seg.resolution = resolution
    if checkBackground:
        maxLabel = np.max(seg)
        print "Checking the background label ..."
        largestCellLabel = np.bincount(seg.flatten()).argmax()
        if largestCellLabel != background:
            np.putmask(seg, seg == background, maxLabel + 1)
            maxLabel += 1
            np.putmask(seg, seg == largestCellLabel, background)
    segImageFName = secondImageFName[:-4] + "_hmin_%d_asf_%1.2f_s_%1.2f_hmin2_%d_asf_%1.2f_s_%1.2f.tif" % (hmin, asf, sigma, hmin_2, asf_2, sigma_2)
    tiffsave(makeImageLabelsConsecutive(seg)[0], segImageFName, **mtags)
    return segImageFName
