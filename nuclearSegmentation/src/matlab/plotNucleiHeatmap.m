function [plt1, plt2, plt3, plt4] = plotNucleiHeatmap(variable, allSelectedLineages, nuclearData, L1_nuclei, resolutions, calculated_quantity, highlighted_lineages)
%PLOTNUCLEIHEATMAP Summary of this function goes here
%   Detailed explanation goes here




gradation=1000;%Defines color range
maxVariable=max(variable(:))%Maximum value for variable
minVariable=min(variable(:))%Minimum value for variable
% minVariable=-0.01%Minimum value for variable

gradationLevel = round(maxVariable*gradation)-(round(minVariable*gradation));%Define color gradient
% colorVector = jet(gradationLevel);%Choose colormap
% colormap(jet);%Set colormap
colorVector = flipud(inferno(gradationLevel));%Choose colormap
colormap(flipud(inferno));%Set colormap
% colorVector = inferno(gradationLevel);%Choose colormap
% colormap(inferno);%Set colormap

time=[0:12:36]; %time vector (change sampling and end point)


% Select lineages which can be used for highlighting. 
% [t4_max, highlighted_lineages] = maxk(allSelectedLineages_aspectRatio(4,:), 5) 

for i=1:4%Loop through each time point
    
    i %counter to follow progress (time point)
    

    figure(i+100)
    clf()


    hold on

    
    for m=1:size(variable(i,:),2)
        
%         m
        a=variable(i,m);
        b=allSelectedLineages(i,m);
        
        if ~isnan(a)
             
            colorRef=round(a*gradation)-round(minVariable*gradation);%define color
%             colorRef= -       round(a*gradation) + round(maxVariable*gradation);%define color
    
            if colorRef==0 || isnan(colorRef)==1 %if value is absent, define to minimum color value
        
                colorRef=1;
        
            end


%             if ~isempty(nuclearData{i,b}) && nuclearData{i,b}.Area ~= 0% && ~isTouchingBorder(nuclearData{i,b}, resolutions(i,:))%if element is not empty
%            t4_labels = L1_nuclei{4}(34:37)

%            if ~isempty(nuclearData{i,b}) && nuclearData{i,b}.Area ~= 0 && ismember(b, lineageCorrespondence(t4_labels, i, allLineageTrace))
           if ~isempty(nuclearData{i,b}) && nuclearData{i,b}.Area ~= 0 && (ismember(m, highlighted_lineages) || isempty(highlighted_lineages))
                
                % Get centroid and orientation and calculate X and Y major
                % and minor axes

%                 centroid = nuclearData{i,b}.Centroid;
%                 orientation = nuclearData{i,b}.Orientation;
%                 xMajor=centroid(1) + [-1 1]*(nuclearData{i,b}.MajorAxisLength/2)*cosd(orientation);
%                 yMajor=centroid(2) - [-1 1]*(nuclearData{i,b}.MajorAxisLength/2)*sind(orientation);
%                 xMinor=centroid(1) - [-1 1]*(nuclearData{i,b}.MinorAxisLength/2)*sind(orientation);
%                 yMinor=centroid(2) - [-1 1]*(nuclearData{i,b}.MinorAxisLength/2)*cosd(orientation);
                
                %plot convex hull defining outter boundary of segmented nucleus
                
                fill(nuclearData{i,b}.ConvexHull(:,1),nuclearData{i,b}.ConvexHull(:,2),colorVector(colorRef,:));
  
            end
        
        end
        
    end

    
    for j=1:size(L1_nuclei{i},1)%Loop through each nucleus in a given time point
            
%         j %counter to follow progress (nucleus number)
            
        a=L1_nuclei{i}(j);
        
        if isnan(a)==0 && a > 0
        
            if isempty(nuclearData{i,a})==0 && nuclearData{i,a}.Area~=0 %&& ~isTouchingBorder(nuclearData{i,a}, resolutions(i,:)) %if element is not empty

                % Get centroid and orientation and calculate X and Y major
                % and minor axes
%                 nuclearData{i,a}

%                 centroid = nuclearData{i,a}.Centroid;
%                 orientation = nuclearData{i,a}.Orientation;
%                 xMajor=centroid(1) + [-1 1]*(nuclearData{i,a}.MajorAxisLength/2)*cosd(orientation);
%                 yMajor=centroid(2) - [-1 1]*(nuclearData{i,a}.MajorAxisLength/2)*sind(orientation);
%                 xMinor=centroid(1) - [-1 1]*(nuclearData{i,a}.MinorAxisLength/2)*sind(orientation);
%                 yMinor=centroid(2) - [-1 1]*(nuclearData{i,a}.MinorAxisLength/2)*cosd(orientation);
                
                %plot convex hull defining outter boundary of segmented nucleus
                
                plot(nuclearData{i,a}.ConvexHull(:,1),nuclearData{i,a}.ConvexHull(:,2),'-k','LineWidth',2);
  
            end
            
        end
        
    end
    
    %set plot formatting variables
    set(gca,'LineWidth',2.5,'FontSize',14);
    xlim([0 max(resolutions(:,1))])
    ylim([0 max(resolutions(:,2))])
    xlabel('x pixel')
    ylabel('y pixel')
    
%     colorbar;

    c = colorbar;%show colorbar
%     colormap(jet);%Set colormap
%     colormap(inferno);%Set colormap
    colormap(flipud(inferno));%Set colormap

    caxis([minVariable maxVariable])
%     caxis([maxVariable minVariable])
        
	%print figure into PDF file (change name as needed)
% 	fname = sprintf('heatmap_CRWN4_pl5_curvature_time%d.pdf',time(i));
% 	print(h, '-dsvg', save_path + "heatmap_" + calculated_quantity + "_t" + time(i) + ".svg")
% 	close(h)
        
end
plt1 = figure(1+100);
plt2 = figure(2+100);
plt3 = figure(3+100);
plt4 = figure(4+100);

end

