function [isTouching] = isTouchingBorder(nucleus,resolution)
%ISTOUCHINGBORDER Check if nuclei touches the image border.
assert(all(size(resolution) == [1, 2]));

maxPixel = max(nucleus.PixelList);
minPixel = min(nucleus.PixelList);
isTouching = false;

if ismember(1, minPixel(1:2))
    isTouching = true;
elseif maxPixel(1) == resolution(1) || maxPixel(2) == resolution(2)
    isTouching = true;
end
end

