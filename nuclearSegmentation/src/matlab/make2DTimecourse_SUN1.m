%%
% ---------------------------------------------------------------------
% Make lineage for an initial cell number
% Forward tracking: select cell at first time point to trace lineage
% forward
% Input:
% lineage_trace - vector/matrix of lineage IDs throughout time course
% data_costanza - costanza data table
% Output:
% lineage_allIntensities - vector/matrix of Costanza expression throughout time course
% lineage_volume - vector/matrix of Costanza volume throughout time course
% ---------------------------------------------------------------------

function [lineage_area,lineage_eccentricity,lineage_aspect_ratio,lineage_area_corrected] = make2DTimecourse_SUN1(lineageTrace,allArea,allEccentricity,allAspectRatio,allArea_corrected)

    for i=1:size(lineageTrace,1)
    
        for j=1:size(lineageTrace,2)
    
            if lineageTrace(i,j) == 1 || lineageTrace(i,j) == 0 || lineageTrace(i,j) == -1
                
                lineage_area(i,j)=NaN;
                
                lineage_aspect_ratio(i,j)=NaN;
                
                lineage_eccentricity(i,j)=NaN;
                
                lineage_area_corrected(i,j)=NaN;

        
            else
        
    
                
                lineage_area(i,j)=allArea(j,lineageTrace(i,j));                         
                
                lineage_aspect_ratio(i,j)=allAspectRatio(j,lineageTrace(i,j));
                
                lineage_eccentricity(i,j)=allEccentricity(j,lineageTrace(i,j));
                
                lineage_area_corrected(i,j)=allArea_corrected(j,lineageTrace(i,j));

            end
    
        end

    end

end