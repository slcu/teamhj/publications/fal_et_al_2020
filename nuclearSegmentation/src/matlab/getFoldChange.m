function [foldChangeMatrix] = getFoldChange(inputMatrix)
%GETFOLDCHANGE calculate proportional changes between timepoints.
% Calculate proportional changes alog the second dimension of a matrix.

foldChangeMatrix = ones(size(inputMatrix,1), size(inputMatrix,2)-1) % Pre-allocate.

for i=1:size(inputMatrix,1)
    for j=1:size(inputMatrix,2)-1
        foldChangeMatrix(i,j)=(inputMatrix(i,j+1)-inputMatrix(i,j))/abs(inputMatrix(i,j));
    end
end
end % function

