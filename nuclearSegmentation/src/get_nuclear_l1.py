from tissueviewer.image import extractNL1UsingMemSegmentation
import numpy as np

img_path = "../images/CRWN4_pl11/"
# membrane_seg_file = img_path + "T1/T1_M_hmin_2_asf_2.00_s_2.00.tif"
# nuclear_seg_file = img_path + "T1/T1_N_hmin_2_asf_2.00_s_2.00_hmin2_2_asf_2.00_s_2.00_bigCellsRemoved_clean_2.tif"
# membrane_seg_file = img_path + "T2/T2_M_hmin_2_asf_2.00_s_2.00.tif"
# nuclear_seg_file = img_path + "T2/T2_N_hmin_2_asf_2.00_s_2.00_hmin2_2_asf_2.00_s_2.00_bigCellsRemoved_40000.tif"
# membrane_seg_file = img_path + "T3/T3_M_hmin_2_asf_2.00_s_2.00.tif"
# nuclear_seg_file = img_path + "T3/T3_N_hmin_2_asf_2.00_s_2.00_hmin2_2_asf_2.00_s_2.00_bigCellsRemoved_40000.tif"
membrane_seg_file = img_path + "T4/T4_M_hmin_2_asf_2.00_s_2.00.tif"
nuclear_seg_file = img_path + "T4/T4_N_hmin_2_asf_2.00_s_2.00_hmin2_2_asf_2.00_s_2.00_bigCellsRemoved.tif"

nl1 = extractNL1UsingMemSegmentation(nuclear_seg_file, membrane_seg_file)

save_name = img_path + "nuclear_l1_labels/T4.csv"
np.savetxt(save_name, nl1, fmt="%i", delimiter=",\n")
