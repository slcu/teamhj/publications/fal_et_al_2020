## Introduction

This repository host the files and software links for performing
analysis used in the paper

Fal, K., Korsbo, N., Alonso-Serra, J., Teles, J., Liu, M., Refahi, Y.,
Chaboute, ME., Jonsson, H., Hamant, O. (2021)
<i>Tissue folding at the organ-meristem boundary results in nuclear
compression and chromatin compaction</i> PNAS

## Files

<tt>nuclearSegmentation</tt> This folder holds the protocol
description and scripts used to
segment and track cells and extract nuclear shapes.

## Data

The confocal data used by the segmentation and analysis is available
via the Cambridge University Data repository (link).

## Contact

Main contact for these segmentation and analysis scripts is henrik.jonsson@slcu.cam.ac.uk.

Main correspondants for the paper: kateryna.fal@cea.fr; henrik.jonsson@slcu.cam.ac.uk, olivier.hamant@ens-lyon.fr.
